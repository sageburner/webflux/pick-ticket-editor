package com.sageburner.editor.pickticket.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.sageburner.editor.pickticket.model.PickTicket
import com.sageburner.editor.pickticket.repository.PickTicketRepository
import org.reactivestreams.Subscription
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import reactor.core.publisher.BaseSubscriber
import java.net.URI
import java.security.SecureRandom


/**
 * PickTicketService
 *
 * Continually generate Pick Ticket requests and send them to configured Sink (REST endpoint, Message Queue, etc.)
 *
 * 1. Get list of all Pick Tickets in the system
 * 2. Select random Pick Tickets from the list to update
 * 3. Update selected Pick Ticket
 * TODO: 4. Request new Pick Ticket list every 5 minutes
 *
 */
@Service
class PickTicketService {

    private val log = LoggerFactory.getLogger(PickTicketService::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    lateinit var pickTicketRepository: PickTicketRepository

    private val mapper = jacksonObjectMapper()

    @Autowired
    lateinit var restTemplate: RestTemplate

    fun start() {
        subscribeToFlux()
    }

    private fun subscribeToFlux() {
        pickTicketRepository.pickTicketFlux.subscribe(object: BaseSubscriber<PickTicket>() {
            override fun hookOnSubscribe(subscription: Subscription ) {
                request(1)
            }

            override fun hookOnNext(pickTicket: PickTicket) {
                val minWait = Integer.parseInt(env.getProperty("app.stream.pickticket.interval.min"))
                val maxWait = Integer.parseInt(env.getProperty("app.stream.pickticket.interval.max"))
                randomWaitMs(minWait, maxWait)
                request(1)
                sendRest(pickTicket)
            }

            override fun hookOnError(throwable: Throwable) {
                log.error("Flux Subscribe Error...")
                super.hookOnError(throwable)
            }
        })
    }

    private fun sendRest(pickTicket: PickTicket) {
        log.info("sendRest .......................")
        val url = String.format("%s/%s?%s", env.getProperty("app.target.rest.url", ""), pickTicket.id, "_method=patch")
        val headers = HttpHeaders()

        env.getProperty("app.target.rest.headers", "")
                .split(",").map {
                    string -> string.split(":")
                }.forEach {
                    slist ->
                        val sarray = slist.toTypedArray()
                        headers.add(sarray[0], sarray[1])
                }

        val request: HttpEntity<PickTicket> = HttpEntity(pickTicket, headers)
        log.info("URL: $url")
        log.info("REQUEST: " + jsonify(request))
//        val response: ResponseEntity<String> = restTemplate.exchange(URI(url), HttpMethod.PATCH, request, String::class.java)
        val response: String = restTemplate.postForObject(url, request, String::class.java) ?: ""
        log.info("RESPONSE: $response")
    }

    private fun randomWait(min: Int, max: Int) {
        val waitTime = (SecureRandom().nextInt(max - min) + min).toLong() * 1000L
        Thread.sleep(waitTime)
    }

    private fun randomWaitMs(min: Int, max: Int) {
        val waitTime = (SecureRandom().nextInt(max - min) + min).toLong()
        Thread.sleep(waitTime)
    }

    private fun jsonify(input: Any): String {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(input)
    }
}