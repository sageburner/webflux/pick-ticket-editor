package com.sageburner.editor.pickticket.model

import java.util.*

/**
 * PickTicket
 *
 */
data class PickTicket(
        val id: UUID,
        val supplyingLocation: Location,
        val requestingLocation: Location,
        val note: String = "note",
        val caseNumber: String?,
        val requisitionLines: List<RequisitionLine>)