package com.sageburner.editor.pickticket.repository

import com.sageburner.editor.pickticket.model.Location
import com.sageburner.editor.pickticket.model.PickTicket
import com.sageburner.editor.pickticket.model.RequisitionLine
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.env.Environment
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.SynchronousSink
import java.net.URI
import java.security.SecureRandom

@Component
class PickTicketRepository : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketRepository::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    lateinit var restTemplate: RestTemplate

    private lateinit var pickTicketList: List<PickTicket>

    lateinit var pickTicketFlux: Flux<PickTicket>

    override fun afterPropertiesSet() {
        log.info("Initializing...")
        buildCache()
        pickTicketList = getPickTickets()
        pickTicketFlux = generatePickTicketsStream()
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    fun getPickTickets(): List<PickTicket> {
        val url = env.getProperty("app.target.rest.url", "")
        val headers = HttpHeaders()

        env.getProperty("app.target.rest.headers", "")
                .split(",").map {
                    string -> string.split(":")
                }.forEach {
                    slist ->
                    val sarray = slist.toTypedArray()
                    headers.add(sarray[0], sarray[1])
                }

        val request: HttpEntity<Any> = HttpEntity(headers)
        val response: ResponseEntity<List<PickTicket>> = restTemplate.exchange(URI(url + "/list?page=1&size=10"), HttpMethod.GET, request, object : ParameterizedTypeReference<List<PickTicket>>() {} )
        log.info("RESPONSE STATUS: " + response.statusCode)

        return response.body ?: emptyList()
    }


    fun generatePickTicketsStream(): Flux<PickTicket> {
        return Flux.generate({ 0 } , {
            index, sink: SynchronousSink<PickTicket> ->
                val pickTicket = getUpdatedPickTicket()
                sink.next(pickTicket)
                index + 1
            })
    }

    /*
     * Update Rules
     *   Can Update:
     *     Supplying Location ID/Name
     *     Note
     *     Case Number
     *     Quantity Needed
     */

    fun getUpdatedPickTicket(): PickTicket {
        val pickTicket = getRandomPickTicket()
        return pickTicket.copy(
                supplyingLocation = getRandomLocation(),
                note = getRandomNote(),
                caseNumber = getRandomCaseNumber(),
                requisitionLines = updateLineQuanties(pickTicket.requisitionLines)
        )
    }

    private val locations: MutableList<Location> = mutableListOf()
    private val notes: MutableList<String> = mutableListOf()

    fun buildCache() {
        log.debug("Reading generator data files")

        // base file dir
        val fileDir = "generatorData"

        // Locations
        var fileName = "locations"
        var resource = ClassPathResource(fileDir + "/" + fileName)
        var resourceInputStream = resource.inputStream

        resourceInputStream.bufferedReader().useLines {
            lines ->
            lines.drop(1).forEach { line ->
                val myLine: Array<String> = line.split(",".toRegex()).toTypedArray()
                val loc = Location(myLine[0], myLine[1], myLine[2].toBoolean())
                locations.add(loc)
            }
        }

        // Notes
        fileName = "notes"
        resource = ClassPathResource(fileDir + "/" + fileName)
        resourceInputStream = resource.inputStream

        resourceInputStream.bufferedReader().useLines { lines -> lines.drop(1).forEach { notes.add(it) } }
    }

    fun getRandomPickTicket(): PickTicket {
        return pickTicketList[SecureRandom().nextInt(pickTicketList.size)]
    }

    fun getRandomLocation(): Location {
        return locations[SecureRandom().nextInt(locations.size)]
    }

    fun getRandomNote(): String {
        return notes[SecureRandom().nextInt(notes.size)]
    }

    fun getRandomCaseNumber(): String {
        return SecureRandom().nextInt(99999).toString()
    }

    fun updateLineQuanties(requisitionLines: List<RequisitionLine>): List<RequisitionLine> {
        return requisitionLines.map {
            it.copy(quantityNeeded = SecureRandom().nextInt(20).toString())
        }
    }

}
