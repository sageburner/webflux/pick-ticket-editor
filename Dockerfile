FROM openjdk:8u282-slim-buster
VOLUME /tmp
ENV spring_profiles_active=test
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app/app.jar"]
